const ProductsModel = require('../models/productsModel');

module.exports = {
    getAll: async (req, res, next) => {
        try{        
            const products = await ProductsModel.find({})
            res.json(products)
        }catch(e){
            next(e)
        }          
    },
    getById: async (req, res, next) => {
        console.log(req.params.id);

        try{
            const product = await ProductsModel.findById(req.params.id)
            res.json(product)
        }catch(e){
            next(e)
        }
    },
    create:async (req, res, next) => {
        console.log(req.body);

        try{
            const product = new ProductsModel({
                name: req.body.name,
                sku: req.body.sku,
                description: req.body.description,
                price: req.body.price,
                quantity:req.body.quantity                
            })
            let newProduct = await product.save()
            res.json(newProduct)
        }catch(e){
            next(e)
        }
        res.send('Products post')
    },
    update:async (req, res, next) => {
        console.log(req.params.id, req.body);

        try{
            let product = await productsModel.update({_id: req.params.id}, req.body, { multi: false})
            res.json(product)
        }catch(e){
            next(e)
        }
        res.send('Product updated');        
    },
    delete:async (req, res, next) => {
        console.log(req.params.id);
        
        try{
            let product = await productsModel.deleteOne({_id: req.params.id})
            res.json(product)
        }catch(e){
            next(e)
        }
        res.send('Product deleted');
    }
}