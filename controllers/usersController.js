const usersModel = require('../models/userModel');
const bcrypt= require('bcrypt');
const jwt= require('jsonwebtoken');

module.exports = {
    
    register:async (req, res, next) => {
        console.log(req.body);

        try{
            let user = await usersModel.create({
                name: req.body.name,
                email: req.body.email,
                passwod: req.body.passwod
            })            
            res.json(user)
        }catch(e){
            next(e)
        }
    },
    login:async (req, res, next) => {
       
        try{
           const user = await usersModel.findOne({email:req.body.email})
           if(user){
             if(bcrypt.compareSync(req.body.passwod, user.passwod)){
                const token = jwt.sign({userId:user._id},req.app.get('scretKey'));
                res.json({token:token})
             }else{
                res.json({error:"El password es incorrecto"}) 
             }
           }else{
               res.json({error:"El mail no está registrado"})
           }
        }catch(e){
            next(e)
        }        
    }   
}