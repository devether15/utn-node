var express = require('express');
var router = express.Router();
const productController = require('../controllers/productsController')

/* GET users listing. */
router.get('/',  (req,res,next)=>{req.app.validateUser(req,res,next)}, productController.getAll);

router.get('/:id', productController.getById);

  router.post('/', productController.create);

  router.put('/:id', productController.update);

  router.delete('/:id', productController.delete);

module.exports = router;