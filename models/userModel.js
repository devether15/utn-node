const mongoose = require('../bin/mongodb')
const bcrypt= require('bcrypt')

const userSchema = new mongoose.Schema({
    name: {
        type:String,
        trim:true,
        required:true
    },
    email:{
        type:String,
        trim:true,
        required:true
    },
    password:{
        type:String,
        trim:true,
        required:true,
    }
})
userSchema.pre('save',function(next){
    const user = this;
    bcrypt.hashSync(user.password,10, function(err,hash){
        if(err){
            return next(err);
        }
        user.password = hash;
        next();
    })
})

module.exports = mongoose.model('users', userSchema)