const mongoose = require('../bin/mongodb')

const { Schema } = mongoose;

const productsSchema = new mongoose.Schema({
    name: String,
    sku: String,
    description: String,
    price: Number,
    quantity: Number
   })

// const productsSchema = new mongoose.Schema({
//     name: {
//         type:String,
//         required:[true,"El name es obligatorio"],
//         minlength:1,
//         maxlength:100
//     },
//     sku: {
//         type:String,
//         required:[true,"El sku es obligatorio"],
//         unique:true
//     },
//     description: String,
//     price: {
//         type:Number,
//         min:1        
//     },
//     quantity:{
//         type:Number,
//         min:1
//     },    
// })

module.exports = mongoose.model('products', productsSchema)